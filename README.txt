DESCRIPTION:
------------
A helper module for Boost. :o)

Boosthelper crawls the URL of a newly created/edited node immediately, instead of waiting for the next cron run.

Works with or without Pathauto. 
Respects node access permissions. (e.g. Private og posts will not be cached.)

DEPENDENCIES:
-------------
Boost	http://drupal.org/project/boost

INSTALLATION
------------
Just enable it. :o)
